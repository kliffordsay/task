SELECT h.name, GROUP_CONCAT(vd.name) distinations
FROM human h
JOIN human_vacation_dist hvd ON h.id = hvd.human_id
JOIN vacation_dist vd ON vd.id = hvd.distination_id
GROUP BY h.name

SELECT DISTINCT h.name
FROM human h
JOIN human_vacation_dist hvd ON h.id = hvd.human_id AND hvd.distination_id = 1
JOIN human_vacation_dist hvd1 ON h.id = hvd1.human_id AND hvd1.distination_id = 3


SELECT h.name
FROM human h
JOIN human_vacation_dist hvd ON h.id = hvd.human_id
GROUP BY h.id
HAVING GROUP_CONCAT(hvd.distination_id ORDER BY hvd.distination_id) = '1,3'