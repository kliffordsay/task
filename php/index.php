<?php
/**
 * Created by PhpStorm.
 * User: kliffsay
 * Date: 3/30/16
 * Time: 12:41 PM
 */

require_once('StringHelper.php');

echo 'string: ';

$handle = fopen ('php://stdin', 'r');
$line = trim(fgets($handle));

echo StringHelper::changeString($line) . PHP_EOL;