<?php

/**
 * Created by PhpStorm.
 * User: kliffsay
 * Date: 3/30/16
 * Time: 12:51 PM
 */
class StringHelper
{
    public static function changeString($string)
    {
        $string  = str_replace(['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'Y', 'y'], '', $string);
        return strrev($string);
    }
}