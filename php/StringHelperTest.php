<?php

/**
 * Created by PhpStorm.
 * User: kliffsay
 * Date: 3/30/16
 * Time: 1:48 PM
 */
require_once('StringHelper.php');

class StringHelperTest extends PHPUnit_Framework_TestCase
{
    public function testHelper()
    {
        $res = StringHelper::changeString('boot');
        $this->assertEquals('tb', $res);

        $res = StringHelper::changeString('Hello World!');
        $this->assertNotEquals('Hello World!', $res);
    }
}